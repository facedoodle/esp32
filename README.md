# FaceDoodle ESP32 Code

This repository contains the code that runs on the ESP32-CAM that connects to the iPad and that controls the Arduino Uno with the CNC Shield (`esp32_main.ino`). For this to work, you will need to install the ESP32 board development library and the ESP32 Servo library.

The `laser_demo.ino` file contains the code used to calibrate the laser.