
#define H 22.5
#define L 3.3
#define delta_alpha 17
#define delta_beta 14
#define tolerance 0.001

//libraries
#include <Servo.h>
#include <math.h>
#include <stdio.h>

Servo servo1; 
Servo servo2;
double alpha;
double beta;

//Serial Serial;

//Circle parameter
const int centerX = 20;
const int centerY = 20;
const int radius = 10;
const int d = 500;


void change_angles(double x, double y);
 
void setup() {
  Serial.begin(9600);
  servo1.attach(9);
  servo2.attach(10);
  change_angles(0,0);
}
 
void loop() {  
  //Terminal
  if (Serial.available()) {
    String input = Serial.readStringUntil('\n');  // Read the input until a newline character is encountered

    // Find the position of the delimiter (comma)
    int delimiterPos = input.indexOf(',');

    if (delimiterPos != -1) {
      // Extract the substrings before and after the delimiter
      String value1Str = input.substring(0, delimiterPos);
      String value2Str = input.substring(delimiterPos + 1);
      
      double c_x = value1Str.toDouble();
      double c_y = value2Str.toDouble();
      change_angles(c_x, c_y);
      delay(1000);
    }
  }
  Serial.print("Diagonale\n");
  //Diagonale
  for (int i = 1; i < 8; ++i) {
    int val = i*5;
    change_angles(val, val);
    delay(d);
  }
  delay(3000);

  Serial.print("Vertical line\n");
  //Vertical line
  for (int i = 1; i < 10; ++i) {
    int val = i*5;
    change_angles(0, val);
    delay(d);
  }
  delay(3000);

  Serial.print("Horizontal line\n");
  //Horizontal line
  for (int i = 1; i < 8; ++i) {
    int val = i*5;
    change_angles(val, 20);
    delay(d);
  }
  delay(3000);

  Serial.print("Circle\n");
  //Circle
  for (int i = 0; i < 100; i++) {
    float angle = 2 * PI * i / 8; // Calculate the angle for each point
    int x = centerX + radius * cos(angle);
    int y = centerY - radius * sin(angle);
    change_angles(x,y);
    delay(50);
  }
  delay(3000);
}

double compute_alpha(double x, double y) {
  double a = atan(x/y);
  a = a*180/PI;
  return a;
}

double compute_beta(double x, double y) {
  double b_1 = asin(L/sqrt(x*x + y*y + H*H));
  double b_2 = atan(H/sqrt(x*x + y*y));
  double b = PI/2 - (b_1 + b_2); 
  b = b*180/PI;
  return b; 
}

void change_angles(double x, double y) {
  //x = map(x, 0, 4, 0, 20.5);
  //y = map(y, 0, 4, 0, 20.5);
  //x += 5;
  //y += 3.5;
  if (abs(x - 0.0) < tolerance && abs(y - 0.0) < tolerance) return;
  alpha = compute_alpha(x,y);
  beta = compute_beta(x,y);
  double a = delta_alpha + alpha;
  double b = 180 + delta_beta - beta;
  servo2.write(round(a));
  servo1.write(round(b));  
}
