#include <analogWrite.h>
#include <ESP32PWM.h>
#include <ESP32Servo.h>
#include <ESP32Tone.h>

/*
  2023, by the FaceDoodle team. 
  EPFL CS-358 Making Intelligent Things.
  Code based on ESP32 Bluetooth LE examples by Neil Kolban, Evandro Copercini and chegewara.
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

// ********************* CONSTANTS *********************

// Bluetooth UUIDs - generated with https://www.uuidgenerator.net/
#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define X_CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
#define Y_CHARACTERISTIC_UUID "ba7ff775-e5d6-4535-ae2d-70dfa683f250"
#define PEN_CHARACTERISTIC_UUID "25a6492e-f044-4021-8de0-3255f82a4545"

// Servo pins
//  • IO15 — Pen Servo
//  • IO14 — Laser 1 Servo
//  • IO2 – Laser 2 Servo
#define SERVO_PEN_PIN (15)
#define SERVO_LASER_ALPHA (14)
#define SERVO_LASER_BETA (2)

// Servo behavior
#define SERVO_ANGLE_UP (180)
#define SERVO_ANGLE_DOWN (10)

// Laser constants
#define H 22.5
#define L 3.3
#define delta_alpha 17
#define delta_beta 14
#define tolerance 0.001

// ********************* VARIABLES **********************

BLECharacteristic *xCharacteristic;
BLECharacteristic *yCharacteristic;
BLECharacteristic *penCharacteristic;

Servo penServo;

Servo servo1; 
Servo servo2;

double alpha;
double beta;

double x_norm = 0, y_norm = 0;
bool pen_up = false;

bool device_connected = false;

// ********************* PROTOTYPES *********************

void change_angles(double x, double y);

class ServerCallbacks: public BLEServerCallbacks {
  void onConnect(BLEServer* pServer) {
    device_connected = true;
  };

  void onDisconnect(BLEServer* pServer) {
    device_connected = false;
    pServer->getAdvertising()->start();
  }
};


// ********************* FUNCTIONS *********************

// Setup

void setup_bt() {
  BLEDevice::init("FaceDoodle Plotter");
  BLEServer *pServer = BLEDevice::createServer();
  BLEService *pService = pServer->createService(SERVICE_UUID);
  pServer->setCallbacks(new ServerCallbacks());
  xCharacteristic = pService->createCharacteristic(
    X_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

  yCharacteristic = pService->createCharacteristic(
    Y_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

  penCharacteristic = pService->createCharacteristic(
    PEN_CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

  xCharacteristic->setValue("0");
  yCharacteristic->setValue("0");
  penCharacteristic->setValue("0");
  pService->start();
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is working for backward compatibility
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
}

void setup_servo() {
  penServo.attach(SERVO_PEN_PIN);
  servo1.attach(SERVO_LASER_ALPHA);
  servo2.attach(SERVO_LASER_BETA);
  
  // Initialize the angles to the origin.
  change_angles(0,0);

}

void setup() {
  Serial.begin(115200, SERIAL_8N1);
  setup_bt();
  setup_servo();
}

// Laser transformation functions

double compute_alpha(double x, double y) {
  double a = atan(x/y);
  a = a*180/PI;
  return a;
}

double compute_beta(double x, double y) {
  double b_1 = asin(L/sqrt(x*x + y*y + H*H));
  double b_2 = atan(H/sqrt(x*x + y*y));
  double b = PI/2 - (b_1 + b_2); 
  b = b*180/PI;
  return b; 
}

/**
 * Sets the angles to the given x, y coordinates.
 */
void change_angles(double x, double y) {
    // Clamp the coordinates to the plotter's bounds
  x = clamp(0, x, 4);
  y = clamp(0, y, 7);

  // Map the coordinates to the given coordinate space
  x = map(x, 0, 4, 0, 20.5);
  y = map(y, 0, 4, 0, 20.5);
  x += 5;
  y += 3.5;
  if (abs(x - 0.0) < tolerance && abs(y - 0.0) < tolerance) return;
  alpha = compute_alpha(x,y);
  beta = compute_beta(x,y);
  double a = delta_alpha + alpha;
  double b = 180 + delta_beta - beta;
  servo2.write(round(a));
  servo1.write(round(b));  
}

void loop() {
  uint8_t *X_val = xCharacteristic->getData();
  uint8_t *Y_val = yCharacteristic->getData();
  uint8_t *P_val = penCharacteristic->getData();
  
  // Evaluate X and Y as doubles
  x_norm = atof((char *) X_val);
  y_norm = atof((char *) Y_val);

  // Evaluate the s
  pen_up = atoi((char *) P_val);

  // Draw on the plotter
  sendGCODE(toGCODE(x_norm, y_norm));
  
  // Set the pen's behaviour
  penServo.write(pen_up ? SERVO_ANGLE_UP : SERVO_ANGLE_DOWN);

  // Set the laser's behavior
  change_angles(x_norm, y_norm);

  // Wait for 200ms (bluetooth frequency)
  delay(200);
}

// ********************* GCODE FUNCTIONS *********************

/**
 * @brief Converts coordinates to GCODE.
 * @param x the x coordinate (in [0, 4])
 * @param y the y coordinate (in [0, 7])
 * @return the GCODE string
*/
String toGCODE(double x, double y) {
  // Clamp the coordinates to the plotter's bounds
  x = clamp(0, x, 4);
  y = clamp(0, y, 7);

  String gcode = "G01 X";

  // Scale the coordinates to the plotter's bounds  
  double newX = x + y - 4;
  double newY = x - y - 4;

  // Append the scaled coordinates to the gcode string
  gcode += newX;
  gcode += " Y";
  gcode += newY;
  gcode += " F300";

  return gcode;
}

/**
 * @brief Sends a GCODE command to the plotter.
 * @param gcode the GCODE command
*/
void sendGCODE(String gcode) {
  Serial.println(gcode);
}

// ********************* UTIL FUNCTIONS *********************

/**
 * @brief Clamps a value between min and max.
 * @param min the lower bound
 * @param val the value to clamp
 * @param max the upper bound
 * @return val in [min, max]
 */
double clamp(double min, double val, double max) {
  double v = val;

  v = v > max ? max : v;
  v = v < min ? min : v;

  return v;
}